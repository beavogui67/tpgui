#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 10 15:53:19 2021

@author: angelina
"""
from __future__ import absolute_import
from tkinter import Button
from tkinter import Entry
from tkinter import Label
from tkinter import messagebox
from tkinter import StringVar
from tkinter import Tk
from tkinter import filedialog

import os


class View(Tk):
    """
    Class which contains the view for the user, interacts with the controller class
    """
    def __init__(self, controller):
        """
        Initialization of the view class
        """
        super().__init__()
        self.controller = controller

        # Creating the main window :
        self.title('Phone book')
        self.option_add("*Dialog.msg.font", "Cambria 20")
        self.option_add("*Button.font", "Cambria 20")

        # Information message as soon as the window is opened
        messagebox.showinfo(title="Warning", message="Hello ! \n\nThe 'Name' "+
                "field is mandatory. \n\nImport the file that contains your contacts,"+
                " if it does not exist create it. \n\nThank you !")

        # Creation of widgets :
        # Texts contained in the different widgets
        ids = ["Name", "First name", "Phone", "Adress", "City"]
        but = ["Search", "Insert", "Delete name"]

        # Creation of dictionaries that will contain the information of the widgets
        self.widgets_labs = {}
        self.widgets_entry = {}
        self.widgets_button = {}

        i = 1
        j = 0

        for idi in ids:
            lab = Label(self, text=idi, font=("Cambria", 20))
            self.widgets_labs[idi] = lab
            lab.grid(row = i, column = 0)

            var = StringVar()
            entry = Entry(self, textvariable=var, font=("Batang", 20), width=40)
            self.widgets_entry[idi] = entry
            entry.grid(row = i, column = 1)

            i += 1

        for idi in but:
            button = Button(self, text=idi, width=20, font=("Batang", 15))
            self.widgets_button[idi] = button
            button.grid(row = i+1, column = j)

            j += 1


        # Adding commands to the different buttons
        self.widgets_button["Insert"].config(command = controller.insert)
        self.widgets_button["Search"].config(command = controller.search)
        self.widgets_button["Delete name"].config(command = controller.delete_name)

        # Creation of the information button
        button_info = Button(self, text="To erase", command = controller.delete_entry,
                             font=("Batang", 14))
        button_info.grid(row= 9, column = 2)

        # Creation of the information button
        button_info = Button(self, text="Show info", command = controller.show_info,
                             font=("Batang", 14))
        button_info.grid(row= 11, column = 2)

        # Creation of the file path button
        button_info = Button(self, text="Open file", command = controller.get_path,
                             font=("Batang", 13))
        button_info.grid(row= 10, column = 1)

    @staticmethod
    def open_file():
        # no-self-use
        """
        Create an interface using the filedialog method that allows to retrieve
        the path of the selected file.

        Returns
        -------
        str
            file path.

        """
        file = filedialog.askopenfilename(initialdir= os.getcwd(),title="Select File",
                                          filetypes=(("all files","*.*"),("Text Files", "*.txt")))
        return str(file)


    def main(self):
        """
        Starting the Event Viewer

        Returns
        -------
        None.

        """
        self.mainloop()
