#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 15 00:13:52 2021

@author: angelina
"""
from __future__ import absolute_import
from tkinter import END
from tkinter import messagebox

from view import View
from model import Individu

class Controller:
    """
    MVC controller class, interacts with the view class and the model class.
    """
    # definition of event handler methods :
    def __init__(self):
        """
        Initialization of the controller class
        """
        self.view = View(self)
        self.individu = Individu("","","","","")
        self.file_name = None


    def get_path(self):
        """
        Retrieves the file path.

        Returns
        -------
        TYPE
            DESCRIPTION.

        """
        if self.file_name is None:
            self.file_name = self.view.open_file()
        return self.file_name


    def write_file(self, dico):
        """
        Method to write to the file

        Parameters
        ----------
        dico : str
            dicotionary containing as key the names and values of the Individual class.

        Returns
        -------
        None.

        """
        # Retrieve the path to the file using the get_path method.
        path = self.get_path()

        # Retrieve the path and open the output file in write mode
        file = open(path,"w")
        for value in dico:
            file.write(str(dico[value]))
        file.close()


    def file_reader(self):
        """
        Method that retrieves a file in read mode, which contains directory data,
        and inserts it into a dictionary.

        Returns
        -------
        dic : str
            contains the data of the file,
            in key: the name of the person
            in value: the Individu class.
        """
        # Retrieve the path to the file using the get_path method.
        path = self.get_path()
        # Opens the file in read mode
        file = open(path, "r")

        # I initialize each variable to empty so that there is always the presence of a value
        name = ""
        firstname = ""
        phone = ""
        adress = ""
        city = ""

        dic = {}

        for line in file:

            # For each line I cut at the ":" level and insert each element in a list.
            # Which in our case gives a list with two elements
            # Exemple :
            #
            # Name : Angelina
            #
            # ['Name','Angelina']
            txt = line.split(":")

            if txt[0].strip() == "Name": # strip removes all spaces present
                # retrieves the second element of the list from the "Name" line
                name = txt[1].strip()

            if txt[0].strip() == "First name":
                firstname = txt[1].strip()

            if txt[0].strip() == "Phone":
                phone = txt[1].strip()

            if txt[0].strip() == "Adress":
                adress = txt[1].strip()

            if txt[0].strip() == "City":
                city = txt[1].strip()

            # if the element is equal to "\n", I know I'm moving on to the next
            # individual record, so I insert the first one in my dictionary
            if txt[0]=="\n":
                classe = Individu(name, firstname, phone, adress, city)
                dic[name.lower()] = classe

        # I return the dictionary in order to use it in the get_fields function.
        return dic


    def insert(self):
        """
        Method that retrieves the data entered by the user and inserts them
        into a dictionary and an output file.

        Returns
        -------
        None.

        """
        # dico calls the "file_reader" function and retrieves all the values of the dic
        dico = self.file_reader()

        # Retrieving input fields using the get() method
        name = self.view.widgets_entry["Name"].get()
        firstname = self.view.widgets_entry["First name"].get()
        phone = self.view.widgets_entry["Phone"].get()
        adress = self.view.widgets_entry["Adress"].get()
        city = self.view.widgets_entry["City"].get()

        # Use of the class (Model)
        classe = Individu(name, firstname, phone, adress, city)

        # Adding data in the dictionary (key = Name) if the name does not exist
        # and if it is not empty
        if name not in dico and name != "":
            dico[name.lower()] = classe
            messagebox.showinfo(title="Successful name insertion", message="The"+
                " following information :" + "\n\n" + str(dico[name.lower()]) + "\n" +
                "have been successfully registered" )
        # If the name is empty, a message is generated
        elif name == "":
            messagebox.showwarning(title="Warning", message="The name is empty !!")

        # If the name does not exist, a message is generated
        else :
            messagebox.showwarning(title="Warning", message="The name already exists !!"+
                                  "\n"+str(dico[name.lower()]))

        # call methods to write to the file and erase fields after insertion
        self.write_file(dico)
        self.delete_entry()


    def fill_entry(self):
        """
        Method that fills the entries with the values of the search key.

        Returns
        -------
        None.

        """
        # dico calls the "file_reader" function and retrieves all the values of the dic
        dico = self.file_reader()
        # Retrieving input fields using the get() method
        name = self.view.widgets_entry["Name"].get()

        # Insert in the widget_entry, the value of the corresponding name key
        self.view.widgets_entry["First name"].insert(0, dico[name].firstname)
        self.view.widgets_entry["Phone"].insert(0, dico[name].phone)
        self.view.widgets_entry["Adress"].insert(0, dico[name].adress)
        self.view.widgets_entry["City"].insert(0, dico[name].city)


    def search(self):
        """
        Method that searches the name in the dictionary keys and generates
        a message if it is found or not.

        Returns
        -------
        None.

        """
        # dico calls the "file_reader" function and retrieves all the values of the dic
        dico = self.file_reader()
        # Retrieving the name search
        name = self.view.widgets_entry["Name"].get()

        if name.lower() in dico.keys() and name != "":
            messagebox.showinfo(title="The name search :", message=dico[name])
        elif name == "":
            messagebox.showwarning(title="Warning", message="The name is empty !!")
        else :
            messagebox.showwarning(title="Warning", message="The name doesn't exist !!")

        # calls the method to fill in the fields after the search
        self.fill_entry()


    def delete_name(self):
        """
        Method that allows you to delete a contact in the phone book, using the dictionary key.

        Returns
        -------
        None.

        """
        # dico calls the "file_reader" function and retrieves all the values of the dic
        dico = self.file_reader()
        # Retrieving the name search
        name = self.view.widgets_entry["Name"].get()

        if name.lower() in dico.keys():
            del dico[name]
            messagebox.showinfo(title="Information", message="The contact has been deleted !!")
        else :
            messagebox.showwarning(title="Warning", message="The name doesn't exist !!")

        # Rewriting the file with the new dico
        self.write_file(dico)
        self.delete_entry()


    def delete_entry(self):
        """
        Method that blanks all fields in the dialog box.
        Except the field that contains the path to the file.

        Returns
        -------
        None.

        """
        for elem in self.view.widgets_entry.values():
            elem.delete(0, END)

    @staticmethod
    def show_info():
        """
        Function that generates a message about the use of the software

        Returns
        -------
        None.

        """
        messagebox.showinfo(title="Warning", message="Hello ! \n\nThe 'Name' "+
                "field is mandatory. \n\nImport the file that contains your contacts,"+
                " if it does not exist create it. \n\nThank you !")


    def main(self):
        """
        Launch the graphical interface

        Returns
        -------
        None.

        """
        self.view.main()


if __name__ == "__main__":
    controller = Controller()
    controller.main()
