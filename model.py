#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 10 15:53:19 2021

@author: angelina
"""

# Model
class Individu:
    """
    Model class of the MVC, interacts with the controller class.
    """
    # pylint: disable=too-few-public-methods
    # pylint: disable=too-many-arguments
    def __init__(self, name, firstname, phone, adress,city):
        """
        Initialization of the Individu class
        """
        self.name = name
        self.firstname = firstname
        self.phone = phone
        self.adress = adress
        self.city = city

    def __str__(self):
        """
        Method to display the contact

        Returns
        -------
        str
            Displays the contact.

        """
        return1 = "Name : "+ self.name +"\n" + "First name : " + self.firstname +"\n"
        return2 = "Phone : " + self.phone +"\n" + "Adress : "+  self.adress +"\n"
        return3 = "City : " + self.city +"\n\n"
        return str(return1 + return2 + return3)
