# INLO TP GUI and MVC

Creation of a graphical interface of an "address book" using the MVC method, where one creates and consults the records of an address and phone book. 
The operation of the application is as follows: 
- the Search button searches for the record corresponding to the name previously entered in the Name field; if successful, the other fields are filled in with information about the name.
- the Insert button fills the selected file with the information previously entered in the various fields, and when you insert it, a confirmation message is displayed.
- the To erase button blanks all the fields in the dialog box.
- the Delete name button deletes the contact in the file by name
- the Open file button allows you to select the file containing the different contacts.
- the Show info button displays a message about using the program.


